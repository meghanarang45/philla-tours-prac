const path = require('path');
const postcssPlugins = [
    require('postcss-simple-vars'),
    require('postcss-nested'),
    require('postcss-import'),
    require('postcss-mixins'),
    require('autoprefixer')
]
module.exports = {
    entry: "./app/assets/scripts/app.js",
    output : {
        filename: "app.bundled.js",
        path: path.resolve(__dirname, 'app')
    },
    devServer: {
        watchFiles: {
            paths: ['./app/**/*.html'],
            options: {
                ignored: './app/assets/*'
            }
        },
        static: {
            directory: path.join(__dirname, "app"),
            watch: false,
        },
        hot: 'only',
        port: 3000
    },
    mode: "development",
    // watch: true,
    module: {
        rules: [
            {
                test: /\.css$/i,
                use: [
                    {
                        loader: 'style-loader'
                    },
                    {
                        loader:'css-loader',
                        options: {
                            url:false,
                        },
                    },
                    {
                        loader: 'postcss-loader',
                        options: {
                            postcssOptions: {
                                plugins: postcssPlugins
                            }
                        },
                    }
                ],
            }
        ]
    },
}