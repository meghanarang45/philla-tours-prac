import $ from 'jquery';
import waypoints from '../../../../node_modules/waypoints/lib/noframework.waypoints';

class DarkHeader{
    constructor(){
        this.siteHeader = $(".header");
        this.headerTriggeringEle = $(".large-hero__title");
        this.headerLogo = $(".header__logo");
        this.createWayPoints();
    }

    createWayPoints(){
        let that = this;
        new Waypoint({
            element: this.headerTriggeringEle[0],
            handler: function(direction){
                if(direction=="down"){
                    that.siteHeader.addClass("header__dark");
                    that.headerLogo.addClass("header__logo-small");
                }else{
                    that.siteHeader.removeClass("header__sark");
                    that.headerLogo.removeClass("header__logo-small");
                }
            },
            offset: "5%"
        });
    }
}

export default DarkHeader;