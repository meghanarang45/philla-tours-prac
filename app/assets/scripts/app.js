import '../styles/style.css'
import $ from 'jquery';
import MobileHeader from './modules/MobileHeader';
import RevealOnScroll from './modules/RevealOnScroll';
import ActiveLinks from './modules/ActiveLinks';
import DarkHeader from './modules/DarkHeader';
import SmoothScroll from './modules/SmoothScroll';
import 'lazysizes';
if(module.hot){
    module.hot.accept()
}
let mobileHeader = new MobileHeader();
new ActiveLinks();
new DarkHeader();
new SmoothScroll();
new RevealOnScroll($(".feature-item"));
new RevealOnScroll($(".testimonial"));

console.log('hi');